# Startup Ideas

Just a brain-dump of ideas I got over the years. Projects that might be developed one day.

## Unswag conferences

Make a community / movement to reduce the amount of swag at conferences. Exchange swag for charity donation. Make sure swag given is not something people never use.

 - https://twitter.com/littlekope/status/1127874187111092224?s=12

## Busses to airports

A lot of airports / cities have a tourist scam scheme where they will put around obstacles to charge unsuspecting tourists a bit extra. Simple info on how to get out/into the airport.

Edinburgh airport needs this *so* much.

## How to move git preserving history

Simple app that will give you code to move history between two hit repos.

## https://justletmeusemyrealname.com/

What it says on the tin. Information for companies on how to handle real names.
